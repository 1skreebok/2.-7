# Задача 2

while True:
    number = str(input('Введите число: '))
    if number.isdigit():
        # ищем числа с остатком 1, но не 11(1, 21,31, 41...1991кот) (не 11 котов)
        if int(number) % 10 == 1 and int(number) % 100 != 11:
            print(str(number) + ' кот!')
        # ищем числа с остатком 2,3,4, но не 12,13,14 (2,3,4,22,23,24,...1994 кота)
        elif int(number) % 10 >= 2 and int(number) % 10 <= 4 \
                and (int(number) % 100 < 10 or int(number) % 100 > 20) and int(number) % 10 != 0:
            print(str(number) + ' кота.')
        else:
            print(str(number) + ' котов.')
    else:
        print(' Введи число от 1 до 2000.')
